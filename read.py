import mraa
import time
 
# analog input - pot
buttonPin = 6
btn = mraa.Gpio(buttonPin)
btn.dir(mraa.DIR_IN) 
 
while True:
    time.sleep(0.2)
    btnVal = btn.read()
    print btnVal   