import requests
import jsonify
import urllib
import json

#simple get getting all tasks
print("simple get getting all tasks:")
response = requests.get("http://127.0.0.1:5000/todo/api/v1.0/tasks")
data = response.json()
print (data)
print()

#get for a specific task
print("get for a specific task:")
response = requests.get("http://127.0.0.1:5000/todo/api/v1.0/tasks/1")
data = response.json()
print (data)
print()
print("ok")

#post of a task:
print("post of the received json:")
data = json.dumps({'done': False, 'description': 'Spaghetti,Pizza,Mandolino', 'title': 'Italians!'})
response = requests.post("http://127.0.0.1:5000/todo/api/v1.0/tasks", "", data)
data = response.json()
print (data)
print (response.reason)
print()

#print("PUT:")
#data = json.dumps({'done': False, 'description': 'Spaghetti,Pizza,Batteria', 'title': 'Italians!'})
#response = requests.put("http://127.0.0.1:5000/todo/api/v1.0/tasks/1", data=data)
#data = response.json()
#print (data)
#print()

print("DELETE:")
response = requests.delete("http://127.0.0.1:5000/todo/api/v1.0/tasks/1")
data = response.json()
print (data)
print()
