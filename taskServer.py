#!flask/bin/python
from flask import Flask, jsonify

#to have a "normal" 404 instead that a flask page
from flask import abort

#flask library to make responses (we use it to jsonify the 404)
from flask import make_response

#flask library to make requests
from flask import request

import json

app = Flask(__name__)

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol', 
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web', 
        'done': False
    }
]

#simple get of all tasks
@app.route('/todo/api/v1.0/tasks', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})
	
	#do this if you want to send urls instead of ids:
	#return jsonify({'tasks': [make_public_task(task) for task in tasks]})
	
#get of a single chosen task (thanks to flask)
@app.route('/todo/api/v1.0/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(404)
    return jsonify({'task': task[0]})

#handler of 404 which jsonify the error
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

#post to get a new task from a client, in json
@app.route('/todo/api/v1.0/tasks', methods=['POST'])
def create_task():
    # in questo modo mi sono stampato tutti gli elementi di request, controllando
    # se fossero callable o attributi (sono tutti attributi string) e il loro
    # valore
    #p.s. credo che puoi usare .get_json() (funzione) invece che .json, ma non ho provato
    #   for attr in dir(request):
    #       print type(attr), "  ", attr, "  ", getattr(request, attr)
    
    #mi sono visto bene come era il json arrivato, era cosi: 
    #   {u'task': 
    #       {u'title': u'Buy groceries', 
    #        u'done': False, 
    #        u'description': u'Milk,Cheese, Pizza, Fruit, Tylenol', 
    #        u'id': 1
    #       }
    #   }
    #   print request.json

    data = json.loads(request.json)
     
    if not data: #or not data.has_key("title"):
        abort(400)
	
    task = {
        'id': tasks[-1]['id'] + 1,
        'title': data['title'],
        #oppure se sei stupidamente sicuro di quello che stai facendo:
        #'title': request.json['title'],
        'description': data['description'],
        'done': False
    }
    tasks.append(task)
    return jsonify({'task': task}), 201

#update
#@app.route('/todo/api/v1.0/tasks/<int:task_id>', methods=['PUT'])
#def update_task(task_id):
#
#    for attr in dir(request):
#        print type(attr), "  ", attr, "  ", getattr(request, attr)
#    
#    print request.form
#    data = json.loads(request.json)
#
#    task = [task for task in tasks if task['id'] == task_id]
#    if len(task) == 0:
#        abort(404)
#    if not data:
#        print("1")
#        abort(400)
#    if 'title' in data and type(data['title']) != unicode:
#        print("2")
#        abort(401)
#    if 'description' in data and type(data['description']) is not unicode:
#        print("3")
#        abort(402)
#    if 'done' in data and type(data['done']) is not bool:
#        abort(403)
#    task[0]['title'] = data.get('title', task[0]['title'])
#    task[0]['description'] = data.get('description', task[0]['description'])
#    task[0]['done'] = data.get('done', task[0]['done'])
#    return jsonify({'task': task[0]})

#delete
@app.route('/todo/api/v1.0/tasks/<int:task_id>', methods=['DELETE'])
def delete_task(task_id):
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(404)
    tasks.remove(task[0])
    return jsonify({'result': True})

#substitute id with url, so that clients do not see ids
def make_public_task(task):
    new_task = {}
    for field in task:
        if field == 'id':
            new_task['uri'] = url_for('get_task', task_id=task['id'], _external=True)
        else:
            new_task[field] = task[field]
    return new_task

if __name__ == '__main__':
    app.run(debug=True)
