'''
Created on 04 dic 2016

@author: K
'''
# input a list from the console_to_str
import future

finished = False
  
myList = []
  
while True:
    a = raw_input("insert new item (insert stop to stop): ")
    if a == "stop":
        break
    else:
        myList.append(a)
  
print("your list is finished.")
    
  
# output a list into the console
  
print("let's print it!")
  
for s in myList:
    print(s)
  
  
# write the list in a file

choice1 = raw_input("do you want to save your list? (y or n)")

if choice1 == "y":
    f = open("myfile", "w")
    
    for s in myList:
        f.write(s)
        f.write("\n")
    
    f.close()

print("the file has been written.")

# read the list from a file

print("let's now read the file!")

myOtherList = []

# with construct! It calls an "__enter__" method at the start, 
# and it surely call an "__exit__" method at the end, 
# with no programming effort!
with open("myfile", "r") as myfile:
    for line in myfile:
        print(line.rstrip())




